import axios from 'axios'

export const baseURL = 'https://17613131-review-dead-archt-yy6okm.server.bonch.dev/'
export const API_URL = baseURL + 'api/'

const API = axios.create({
  baseURL: API_URL
})

export default API
