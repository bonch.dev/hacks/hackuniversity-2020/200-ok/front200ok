import Vue from 'vue'
import textareaAutosize from 'vue-textarea-autosize'

Vue.use(textareaAutosize)
