import API from '../../boot'

const state = {
  catalogue: [],
  types: [],
  art: {}
}
const getters = {
  catalogue: state => state.catalogue,
  art: state => state.art,
  types: state => state.types
}
const mutations = {
  SET_CATALOGUE (state, payload) {
    state.catalogue = Object.values(payload)
  },
  SET_ART (state, payload) {
    state.art = Object.values(payload)
  },
  SET_TYPES (state, payload) {
    state.types = Object.values(payload)
  }
}
const actions = {
  fetchTypes ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('catalog/types')
        .then(response => {
          commit('SET_TYPES', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  fetchCatalogue ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get('catalog/types/' + payload + '/products')
        .then(response => {
          commit('SET_CATALOGUE', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  fetchArt ({ commit }, slug) {
    return new Promise((resolve, reject) => {
      API.get(`art/${slug}`)
        .then(response => {
          commit('SET_CATALOGUE', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
