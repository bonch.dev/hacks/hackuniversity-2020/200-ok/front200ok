import API from '../../boot'

const state = {
  sameProducts: [],
  artistProducts: []
}
const getters = {
  sameProducts: state => state.sameProducts,
  artistProducts: state => state.artistProducts
}
const mutations = {
  SET_SAME_PRODUCTS (state, payload) {
    state.sameProducts = payload
  },
  SET_ARTIST_PRODUCTS (state, payload) {
    state.artistProducts = payload
  }
}
const actions = {
  fetchSameProducts ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get('arts/' + payload)
        .then(response => {
          commit('SET_SAME_PRODUCTS', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  fetchArtistProducts ({ commit }, payload) {
    return new Promise((resolve, reject) => {
      API.get('artists/' + payload + '/products')
        .then(response => {
          commit('SET_ARTIST_PRODUCTS', response.data.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
