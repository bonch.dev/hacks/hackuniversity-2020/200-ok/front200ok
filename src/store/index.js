import Vue from 'vue'
import Vuex from 'vuex'
import ProductModule from './ProductModule'
import AuthModule from './AuthModule'
import CartModule from './CartModule'
import CatalogueModule from './CatalogueModule'
import ProfileModule from './ProfileModule'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      ProductModule, AuthModule, CartModule, CatalogueModule, ProfileModule
    },
    strict: process.env.DEV
  })

  return Store
}
