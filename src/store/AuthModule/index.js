import API, { baseURL } from '../../boot'
import axios from 'axios'

const state = {
  token: null
}
const getters = {
  token: state => state.token
}
const mutations = {
  AUTH_SUCCESS (state, payload) {
    state.token = payload
  }
}
const actions = {
  signIn ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.get(baseURL + 'sanctum/csrf-cookie', {
        withCredentials: true
      })
        .then((Response) => {
          return new Promise((resolve, reject) => {
            API.post('auth/signin ', data)
              .then((Response) => {
                const token = Response.data.token
                commit('AUTH_SUCCESS', Response.token)
                localStorage.setItem('userToken', token)
                API.defaults.headers.common.authorization = 'Bearer' + token
                resolve(Response)
              }).catch(error => { reject(error) })
          })
        }).catch(error => {
          reject(error)
        })
    })
  },
  signUp ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.get(baseURL + 'sanctum/csrf-cookie', {
        withCredentials: true
      })
        .then((Response) => {
          return new Promise((resolve, reject) => {
            API.post('auth/signup ', data)
              .then((Response) => {
                const token = Response.data.token
                API.defaults.headers.common.authorization = 'Bearer' + token
                resolve(Response)
              }).catch(error => { reject(error) })
          })
        }).catch(error => { reject(error) })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
