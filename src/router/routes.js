
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout'),
    children: [
      {
        path: '',
        component: () => import('pages/Index')
      },
      {
        path: 'catalogue',
        component: () => import('pages/Catalogue.vue')
      },
      {
        path: 'product/:product',
        name: 'product',
        component: () => import('pages/Product.vue')
      },
      {
        path: 'artist',
        component: () => import('pages/Artist')
      },
      {
        path: 'new-order',
        component: () => import('pages/Neworder')
      },
      {
        path: 'All-artist',
        component: () => import('pages/Allartist')
      },
      {
        path: 'collection',
        component: () => import('pages/Collection')
      },
      {
        path: 'collections',
        component: () => import('pages/Collections')
      },
      {
        path: 'profile',
        component: () => import('pages/Profile'),
        children: [
          {
            path: '/orders'
            // component: () => import('pages/componentName')
          },
          {
            path: '/commissions'
            // component: () => import('pages/componentName')
          },
          {
            path: '/edit'
            // component: () => import('pages/componentName')
          },
          {
            path: '/artist',
            children: [
              {
                path: '/commissions'
                // component: () => import('pages/componentName')
              },
              {
                path: '/drops'
                // component: () => import('pages/componentName')
              },
              {
                path: '/prints'
                // component: () => import('pages/componentName')
              }
            ]
          }
        ]
      },
      {
        path: 'cart',
        component: () => import('pages/Cart')
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/AuthLayout'),
    children: [
      {
        path: 'signin',
        component: () => import('pages/SignIn')
      },
      {
        path: 'register',
        component: () => import('pages/Register')
      }
    ]
  }

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
